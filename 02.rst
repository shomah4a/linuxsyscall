=======================================================================
 第(1+1)回 六本木 Linux カーネル読書会 〜 プロセス管理 ロード第一章 〜
=======================================================================

clone/fork とか読みます。

`connpass <http://connpass.com/event/824/>`_

`vfork の man <http://linuxjm.sourceforge.jp/html/LDP_man-pages/man2/vfork.2.html>`_

`togetter <http://togetter.com/li/348979>`_

`@draftcode <http://twitter.com/draftcode>`_ 氏による `まとめ <http://draftcode.github.com/2012/08/01/5524f2e3-1431-4ee0-b04c-ab89ebdcdf4a.html>`_

やったこと
==========

ご挨拶
======
- 開催目的
  開催者が勉強するため
- 参加者と主催者ではなくてみんなで読みましょう

前回のおさらい
==============
前回はモヒモヒ
システムコール読書会がとっかかりとして微妙だったので rename して再出発

概要
====
- fork, vfork, clone で生成
- execve で新しいプロセスとして実行
- exit で終了

clone
-----
- do_fork が本体
- 親の子プロセスとして生成

execve
------
- 実行ファイルのバイナリをロードする
- do_execve が本体

exit
----
- exit/exit_group 実行を終了する

コードを読む
============

clone 関数
----------

`arch/ia64/kernel/entry.S <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=blob;f=arch/ia64/kernel/entry.S;h=1ccbe12a4d84fe6585455a45b367647b1fa2c240;hb=ff74ae50f01ee67764564815c023c362c87ce18b>`_

アセンブラの命令群

do_fork 関数
------------
`kernel/fork.c:1544 <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=blob;f=kernel/fork.c;h=81633337aee17c924ed7469432e25e9b7c1f7f7e;hb=ff74ae50f01ee67764564815c023c362c87ce18b#l1545>`_

process.c から呼ばれているらしい

int __user *parent_tidptr, int __user *child_tidptr

thread_id へのポインタ

__user
~~~~~~
include/linux/kernel.h
メモリ空間に関するオプションらしい。

__kernel

.. note:: vfork, fork, clone の違いについて

   - clone と違い vfork では親のメモリをコピーしない
   - fork と違い vfork では親の実行をブロックする

   http://surf.ml.seikei.ac.jp/~nakano/JMwww/html/LDP_man-pages/man2/vfork.2.html

   歴史的な違い

do_fork を読む
--------------
`kernel/fork.c:1560 <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=blob;f=kernel/fork.c;h=81633337aee17c924ed7469432e25e9b7c1f7f7e;hb=ff74ae50f01ee67764564815c023c362c87ce18b#l1560>`_

権限チェックなど

clone_flags
~~~~~~~~~~~

`include/linux/sched.h <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=blob;f=include/linux/sched.h;h=81a173c0897d91c2d5b22b5370d282c7346bd8a9;hb=ff74ae50f01ee67764564815c023c362c87ce18b>`_ あたりに書いてある

CLONE_NEWUSER かつ CLONE_THREAD

copy_process
~~~~~~~~~~~~
プロセスをコピーするところ

`kernel/fork.c:1560 <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=blob;f=kernel/fork.c;h=81633337aee17c924ed7469432e25e9b7c1f7f7e;hb=ff74ae50f01ee67764564815c023c362c87ce18b#l117>`_

dup_task_struct
~~~~~~~~~~~~~~~
タスク情報をコピーするところ

`kernel/fork.c:1560 <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=blob;f=kernel/fork.c;h=81633337aee17c924ed7469432e25e9b7c1f7f7e;hb=ff74ae50f01ee67764564815c023c362c87ce18b#l256>`_

プロセスのメモリ管理
~~~~~~~~~~~~~~~~~~~~
メモリ領域の確保

- alloc_task_struct_node

  - `kernel/fork.c:1560 <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=blob;f=kernel/fork.c;h=81633337aee17c924ed7469432e25e9b7c1f7f7e;hb=ff74ae50f01ee67764564815c023c362c87ce18b#l116>`_

- kmem_cache_alloc_node

  - `mm/slab.c:3791 <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=blob;f=mm/slab.c;h=e901a36e2520c2b7aa5123ddcd0553415b764397;hb=ff74ae50f01ee67764564815c023c362c87ce18b#l3791>`
  - `mm/slob.c:609 <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=blob;f=mm/slob.c;h=8105be42cad13b9ba6d231de8fad5bf29af6de2f;hb=ff74ae50f01ee67764564815c023c362c87ce18b#l609>`_
  - `mm/slub.c:2403 <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=blob;f=mm/slub.c;h=71de9b5685fa60a9d6e9a80ba906efd10bb64e5c;hb=ff74ae50f01ee67764564815c023c362c87ce18b#l2403>`_

GFP_KERNEL って何よ?
++++++++++++++++++++
メモリ確保時のオプション

gfp.h
#define GFP_KERNEL	(__GFP_WAIT | __GFP_IO | __GFP_FS)
get_free_page

http://www.mech.tohoku-gakuin.ac.jp/rde/contents/linux/drivers/tips1.html

sched_fork
~~~~~~~~~~
スケジューラに登録する

タスクの状態を TASK_RUNNING にしている

`kernel/sched/core.c:1732 <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=blob;f=kernel/sched/core.c;h=2000e069fc9e8844ce14395f0fc1cb24e699aa10;hb=ff74ae50f01ee67764564815c023c362c87ce18b#l1732>`_

set_task_cpu
~~~~~~~~~~~~
タスクを動かす CPU を決定

`kernel/sched/core.c:1083 <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=blob;f=kernel/sched/core.c;h=2000e069fc9e8844ce14395f0fc1cb24e699aa10;hb=ff74ae50f01ee67764564815c023c362c87ce18b#l1083>`_

get_cpu
~~~~~~~
使う CPU を決定する

`include/linux/smp.h:219 <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=blob;f=include/linux/smp.h;h=10530d92c04b05bd56ab269974094ecfd2efbaab;hb=ff74ae50f01ee67764564815c023c362c87ce18b#l219>`_

IA64 の場合は最終的に以下のような定義になっている。

`arch/ia64/include/asm/smp.h:51 <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=blob;f=arch/ia64/include/asm/smp.h;h=0b3b3997decd1adf397901a4ec4b4ad9d3328cfe;hb=ff74ae50f01ee67764564815c023c362c87ce18b#l51>`_

#define raw_smp_processor_id() (current_thread_info()->cpu)

今のプロセスと同じ CPU を使っている?

ftrace_graph_init_task
~~~~~~~~~~~~~~~~~~~~~~
rcu_copy_process
~~~~~~~~~~~~~~~~
`リード・コピー・アップデート <http://ja.wikipedia.org/wiki/%E3%83%AA%E3%83%BC%E3%83%89%E3%83%BB%E3%82%B3%E3%83%94%E3%83%BC%E3%83%BB%E3%82%A2%E3%83%83%E3%83%97%E3%83%87%E3%83%BC%E3%83%88>`_

`カーネル内部のLock-freeというかRCUの話 <http://togetter.com/li/153033>`_

rcu 用の領域を確保

wake_up_task
~~~~~~~~~~~~
run queue に登録するところ

`kernel/sched/core.c:1813 <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=blob;f=kernel/sched/core.c;h=2000e069fc9e8844ce14395f0fc1cb24e699aa10;hb=ff74ae50f01ee67764564815c023c362c87ce18b#l1813>`_

ここでも set_task_cpu を読んでいる

タスクスケジューラに変更があったために色々と変わっているようだ

http://itpro.nikkeibp.co.jp/article/COLUMN/20080507/300814/

struct rq
~~~~~~~~~
CPU ごとに持つ runqueue の構造体

`include/linux/sched.h:347 <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=blob;f=include/linux/sched.h;h=81a173c0897d91c2d5b22b5370d282c7346bd8a9;hb=ff74ae50f01ee67764564815c023c362c87ce18b#l347>`_

activate_task
~~~~~~~~~~~~~
run queue に登録するところ
~~~~~~~~~~~~~~~~~~~~~~~~~~
`kernel/sched/core.c:1813 <http://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git;a=blob;f=kernel/sched/core.c;h=2000e069fc9e8844ce14395f0fc1cb24e699aa10;hb=ff74ae50f01ee67764564815c023c362c87ce18b#l729>`_

do_execve
---------

fs/exec.c

次回やること
============
次回 execve

担当は @mizon8
  
キーワード
==========

- fork/vfork/clone
- struct task_struct
- プロセスの名前空間
- Process
- DMA (Direct Memory Access)
- CoW (Copy-on-Write)
- RCU (Read-Copy-Update)
- runqueue


