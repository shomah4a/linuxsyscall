.. Linux システムコール読書会 documentation master file, created by
   sphinx-quickstart on Mon Jul  2 13:43:28 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

六本木 Linux カーネル読書会
===========================

申し込みなどは `connpass のシリーズページ <http://connpass.com/series/134/>`_ からどうぞ

読んでいるソースは :doc:`files` から

.. toctree::
   :maxdepth: 2

   01
   02
   03
   04
   files



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

